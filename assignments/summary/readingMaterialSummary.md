## IoT
IoT is short for Internet of Things. The Internet of Things refers to the ever-growing network of physical objects that feature an IP address for internet connectivity, and the communication that occurs between these objects and other internet-enabled devices and systems. 

## IIoT
Industrial Internet of Things (IIoT) is a subset of IoT, aimed specifically at industrial applications. IIoT is about connecting machines to other machines/data management and the optimization and productivity that is possible to make “smart factories.

<img src="https://www.cdnsol.com/blog/wp-content/uploads/2017/03/IIoT-Solutions-CeBIT-Hannover-2017.png" width="400"  heigth="400">
 
## Industrial Revoution

**1st industrial revolution** – Steam and water power are used to mechanize production

**2nd industrial revolution** – Electricity allows for mass production with assembly lines

**3rd industrial revolution** – IT and computer technology are used to automate processes

**4th industrial revolution (Industry 4.0)** – Enhancing automation and connectivity with Cyber Physical Systems

<img src="https://cdn2.slidemodel.com/wp-content/uploads/13049-01-industry-4-2.jpg" width="400"  heigth="400">

## Industry 3.0
It was during this period of transformation era where more and more robots were used in to the processes to perform the tasks which were performed by humans. E.g. Use of Programmable Logic Controllers, Robots etc.
The Data is stored in databases and represented in excel sheets.

## Industry 3.0 Architecture

Sensors --> PLC's --> SCADA & ERP

Sensors installed at various points in the Factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data .Usually this data is stored in Excels and CSV's and rearely get plotted as real-time graphs or charts.

<img src="https://gitlab.com/ishitanaik99/iotmodule1/-/raw/master/extras/3.0_Architecture.png" width="400"  heigth="400">

## Industry 3.0 Communication Protocols

All these protocols are optimized for sending data to a central server inside the factory. These protocols are used by sensors to Send data to PLC's. These protocols are called as fieldbus.

Few protocols are mentioned below-
1. Modbus
2. Profinet
3. CANopen
4. EtherCAT

## Industry 4.0 

Industry 4.0 is _Industry 3.0_ connected to Internet. Connecting to Internet makes data communication faster, easier without data loss.
The data collected can be used for various purposes:
1. Showing Dashboards
2. Remote Control configuration of devices
3. Real time event stream processing
4. Remote Web SCADA
5. Predictive maintenance
6. Analytics with predictive models
7. Automated device provisioning
8. Real-time alerts & alarms


## Indsutry 4.0 Architecture

IoT Gateway acts as a bridge between PLCs and SCADA, converts the data received from them and send to Cloud using various protocols like MQTT, CoAP.
Edge gets the data from controllers and convert it to a cloud understandable protocol.

<img src="https://gitlab.com/ishitanaik99/iotmodule1/-/raw/master/extras/industry-4_arch.png" width="800"  heigth="800">


## Industry 4.0 Communication Protocols
All these protocols are optimized for sending data to cloud for data analysis.
1.MQTT 

2. AMQP

3. CoAP

4. HTTP

5. RESTful API

6. Websockets

<img src="https://cms.spectra.de/fileadmin/_processed_/d/c/csm_IoT_Automationpyramide_en_337b49668f.jpg" width="400"  heigth="400">

## Problems for Industry 4.0 Upgrades

- **Cost:** Factory owners don't want to switch into Industry 4.0, because it is very expensive.
- **Downtime:** Changing Hardware would result in downtime and nobody want to face such loss.
- **Reliability:** Investing in devices which are unproven and unreliable is a huge risk for any business.

## Solution:

The solution is to : Get data from the devices already present in the factory and send it to cloud.
So, there is no need to replace the original devices, therefore lower risk factor.
Thus Industry 3.0 protocols should be converted to Industry 4.0 protocols.

However some Challenges faced in this conversion are :
1. Expensive Hardware
2. Lack of documentation
3. Propritary PLC Protocols
## How to Convert

There is a library known as Shunaya Interface that helps get data from Industry 3.0 devices and send to Industry 4.0 cloud.
<img src="https://gitlab.com/ishitanaik99/iotmodule1/-/raw/master/extras/iiot.png"  heigth="400">


## Steps to make an IIoT Product

- Identify most popular Industry 3.0 devices.
- Study Protocols that these devices Communicate.
- Get data from the Industry 3.0 devices.
- Send the data to cloud for Industry 4.0.
  
## Industry 3.0 vs Industry 4.0
The basic difference is that the machines work autonomously without the intervention of a human in Industry 4.0. Whereas in the industry 3.0 the machines are only automatized.
<img src="https://gitlab.com/ishitanaik99/iotmodule1/-/raw/master/extras/3.0_to_4.0.png" width="400"  heigth="400">


## After Sending Data to Industry 4.0 cloud

After sending data to cloud, Data Analysis is carried out with the help of several tools.
Data is stored in Time series databases(TSDB)

Various TSDB tools are :
1. INFLUX db
2. Promethus
3. Things Board
4. Grafana

These data can be stored in IoT platforms like 
1. AWS
2. Azure
3. Google 
4. Thingsboard
